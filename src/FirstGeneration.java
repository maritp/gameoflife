import java.util.Random;

class FirstGeneration {

    static void checkCellNumber(int x, LivingMap livingMap) {

        if (x == 9) {
            OriginalMap originalMap = new OriginalMap().invoke();
            Rules.applyingRules(x, originalMap.set, livingMap);
        }

        if (x == 8) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(3, originalMap);
            replaceCells(4, originalMap);
            replaceCells(5, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }

        if (x == 7) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(1, originalMap);
            replaceCells(2, originalMap);
            replaceCells(3, originalMap);
            replaceCells(4, originalMap);
            replaceCells(5, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }

        if (x == 6) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(1, originalMap);
            replaceCells(2, originalMap);
            replaceCells(3, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }

        if (x == 5) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(1, originalMap);
            replaceCells(2, originalMap);
            replaceCells(3, originalMap);
            replaceCells(7, originalMap);
            replaceCells(8, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }

        if (x == 4) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(1, originalMap);
            replaceCells(7, originalMap);
            replaceCells(8, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }

        if (x == 3) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(1, originalMap);
            replaceCells(5, originalMap);
            replaceCells(6, originalMap);
            replaceCells(7, originalMap);
            replaceCells(8, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }

        if (x == 2) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(5, originalMap);
            replaceCells(6, originalMap);
            replaceCells(7, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }
        if (x == 1) {
            OriginalMap originalMap = new OriginalMap().invoke();

            replaceCells(3, originalMap);
            replaceCells(4, originalMap);
            replaceCells(5, originalMap);
            replaceCells(6, originalMap);
            replaceCells(7, originalMap);

            Rules.applyingRules(x, originalMap.set, livingMap);
        }
        if (x == 0) {
            System.out.println("SWITCHING GENERATIONS!");
            x = 9;
            new Rules().applyingRules(x, livingMap.livingMap.set, 2);
        }
    }

    private static void replaceCells(int x, OriginalMap originalMap) {
        Random number = new Random();
        int random = number.nextInt(2);

        originalMap.set.replace(x, originalMap.set.get(x), random);
    }
}