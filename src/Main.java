public class Main {

    public static void main(String[] args) {
        OriginalMap originalMap = new OriginalMap().invoke();
        LivingMap livingMap = new LivingMap();
        int x = originalMap.getX();

        FirstGeneration.checkCellNumber(x, livingMap);
    }
}