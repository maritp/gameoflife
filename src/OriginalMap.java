import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

class OriginalMap {
    private int x = 1;
    Map<Integer, Integer> set = new TreeMap<>();
    private Random number = new Random();
    private int random = number.nextInt(2);

    int getX() {
        return x;
    }

    OriginalMap invoke() {
        set.put(x, random);
        set.put(++x, random);
        set.put(++x, random);
        set.put(++x, random);
        set.put(++x, random);
        set.put(++x, random);
        set.put(++x, random);
        set.put(++x, random);
        set.put(++x, random);

        return this;
    }
}