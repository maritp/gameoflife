import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

class OtherGenerations {
    private static int y9;
    private static int y8;
    private static int y7;
    private static int y6;
    private static int y5;
    private static int y4;
    private static int y3;
    private static int y2;
    private static int y1;

    private static Map<Integer, Integer> map8;
    private static Map<Integer, Integer> map7;
    private static Map<Integer, Integer> map6;
    private static Map<Integer, Integer> map5;
    private static Map<Integer, Integer> map4;
    private static Map<Integer, Integer> map3;
    private static Map<Integer, Integer> map2;

    private Rules currentGeneration = new Rules();
    private static int generationCounter = 0;

    void checkCellNumber(int x, Map<Integer, Integer> set, int generationCounter) {

        Random number = new Random();
        int random = number.nextInt(2);

        if (generationCounter != OtherGenerations.generationCounter) {
            currentGeneration = new Rules();
            OtherGenerations.generationCounter = generationCounter;
        }

        if (x == 8) {
            y9 = set.get(9);
            y8 = set.get(8);
            y7 = set.get(7);
            y6 = set.get(6);
            y5 = set.get(5);
            y4 = set.get(4);
            y3 = set.get(3);
            y2 = set.get(2);
            y1 = set.get(1);

            Map<Integer, Integer> set8 = new TreeMap<>();
            set8.put(9, y9);
            set8.put(8, y8);
            set8.put(7, y7);
            set8.put(6, y6);
            set8.put(5, random);
            set8.put(4, random);
            set8.put(3, random);
            set8.put(2, y2);
            set8.put(1, y1);

            currentGeneration.applyingRules(x, set8, OtherGenerations.generationCounter);
        }

        if (x == 7) {
            y8 = set.get(8);
            map8 = set;

            Map<Integer, Integer> set7 = new TreeMap<>();
            set7.put(9, set.get(9));
            set7.put(8, set.get(8));
            set7.put(7, set.get(7));
            set7.put(6, set.get(6));
            set7.put(5, set.get(5));
            set7.put(4, set.get(4));
            set7.put(3, random);
            set7.put(2, random);
            set7.put(1, random);

            currentGeneration.applyingRules(x, set7, OtherGenerations.generationCounter);
        }

        if (x == 6) {
            y7 = set.get(7);
            map7 = set;

            Map<Integer, Integer> set6 = new TreeMap<>();
            set6.put(9, set.get(9));
            set6.put(8, set.get(8));
            set6.put(7, set.get(7));
            set6.put(6, set.get(6));
            set6.put(5, y5);
            set6.put(4, y4);
            set6.put(3, set.get(3));
            set6.put(2, random);
            set6.put(1, set.get(1));

            currentGeneration.applyingRules(x, set6, OtherGenerations.generationCounter);
        }

        if (x == 5) {
            y6 = set.get(6);
            map6 = set;

            Map<Integer, Integer> set5 = new TreeMap<>();
            set5.put(9, set.get(9));
            set5.put(8, random);
            set5.put(7, random);
            set5.put(6, set.get(6));
            set5.put(5, set.get(5));
            set5.put(4, set.get(4));
            set5.put(3, random);
            set5.put(2, set.get(2));
            set5.put(1, set.get(1));

            currentGeneration.applyingRules(x, set5, OtherGenerations.generationCounter);
        }

        if (x == 4) {
            y5 = set.get(5);
            map5 = set;

            Map<Integer, Integer> set4 = new TreeMap<>();
            set4.put(9, set.get(9));
            set4.put(8, set.get(8));
            set4.put(7, set.get(7));
            set4.put(6, set.get(6));
            set4.put(5, set.get(5));
            set4.put(4, set.get(4));
            set4.put(3, y3);
            set4.put(2, y2);
            set4.put(1, random);

            currentGeneration.applyingRules(x, set4, OtherGenerations.generationCounter);
        }

        if (x == 3) {
            y4 = set.get(4);
            map4 = set;

            Map<Integer, Integer> set3 = new TreeMap<>();
            set3.put(9, set.get(9));
            set3.put(8, set.get(8));
            set3.put(7, random);
            set3.put(6, random);
            set3.put(5, random);
            set3.put(4, set.get(4));
            set3.put(3, set.get(3));
            set3.put(2, set.get(2));
            set3.put(1, set.get(1));

            currentGeneration.applyingRules(x, set3, OtherGenerations.generationCounter);
        }

        if (x == 2) {
            y3 = set.get(3);
            map3 = set;

            Map<Integer, Integer> set2 = new TreeMap<>();
            set2.put(9, set.get(9));
            set2.put(8, y8);
            set2.put(7, set.get(7));
            set2.put(6, random);
            set2.put(5, set.get(5));
            set2.put(4, set.get(4));
            set2.put(3, set.get(3));
            set2.put(2, set.get(2));
            set2.put(1, y1);

            currentGeneration.applyingRules(x, set2, OtherGenerations.generationCounter);
        }
        if (x == 1) {
            y2 = set.get(2);
            map2 = set;

            Map<Integer, Integer> set1 = new TreeMap<>();
            set1.put(9, set.get(9));
            set1.put(8, set.get(8));
            set1.put(7, random);
            set1.put(6, set.get(6));
            set1.put(5, set.get(5));
            set1.put(4, map8.get(4));
            set1.put(3, map8.get(3));
            set1.put(2, set.get(2));
            set1.put(1, set.get(1));

            currentGeneration.applyingRules(x, set1, OtherGenerations.generationCounter);
        }
        if (x == 0) {
            y1 = set.get(1);

            Map<Integer, Integer> map9New = new TreeMap<>();
            map9New.put(9, y9);
            map9New.put(8, y8);
            map9New.put(7, y7);
            map9New.put(6, y6);
            map9New.put(5, y5);
            map9New.put(4, y4);
            map9New.put(3, y3);
            map9New.put(2, y2);
            map9New.put(1, y1);

            String rowX2 =
                    map5.get(3) + "" + map6.get(2) + "" + map6.get(1) + "" + map7.get(3) + "" + map7.get(2);
            String rowX1 =
                    map5.get(7) + "" + map4.get(5) + "" + map5.get(6) + "" + map6.get(7) + "" + map7.get(5);
            String rowX =
                    map3.get(8) + "" + map3.get(4) + "" + map2.get(9) + "" + map6.get(8) + "" + map7.get(4);
            String rowXm1 =
                    map3.get(1) + "" + map3.get(3) + "" + map2.get(2) + "" + map8.get(1) + "" + map8.get(3);
            String rowXm2 =
                    map3.get(6) + "" + map3.get(7) + "" + map3.get(5) + "" + map2.get(6) + "" + set.get(7);

            System.out.println("\nGeneration: " + generationCounter);
            System.out.println(rowX2);
            System.out.println(rowX1);
            System.out.println(rowX);
            System.out.println(rowXm1);
            System.out.println(rowXm2);

            synchronized (this) {
                try {
                    this.wait(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            x = 9;
            currentGeneration.applyingRules(x, map9New, (generationCounter + 1));
        }
    }
}