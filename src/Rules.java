import java.util.Map;

class Rules {

    private static OtherGenerations otherGen = new OtherGenerations();

    void applyingRules(int x, Map<Integer, Integer> set, int generationCounter)  {
        int liveCellsOverall = 0;

        for (Map.Entry<Integer, Integer> entry : set.entrySet()) {
            liveCellsOverall = liveCellsOverall + entry.getValue();
        }
        int liveCells = +liveCellsOverall - set.get(x);

        if (liveCells < 2 && set.get(x) == 1) {
                set.replace(x, set.get(x), 0);
        }
        if (liveCells == 3 && set.get(x) == 0) {
                set.replace(x, set.get(x), 1);
        }
        if (liveCells > 3 && set.get(x) == 1) {
                set.replace(x, set.get(x), 0);
        }
        otherGen.checkCellNumber(--x, set, generationCounter);
    }

    static void applyingRules(int x, Map<Integer, Integer> set, LivingMap livingMap) {

        int liveCellsOverall = 0;

        for (Map.Entry<Integer, Integer> entry : set.entrySet()) {
            liveCellsOverall = liveCellsOverall + entry.getValue();
        }
        int liveCells = +liveCellsOverall - set.get(x);

        if (liveCells < 2 && set.get(x) == 1) {
                cellDiesFirstGen(x, livingMap);
        }
        if (liveCells == 3 && set.get(x) == 0) {
                cellLivesFirstGen(x, livingMap);
        }
        if (liveCells > 3 && set.get(x) == 1) {
                cellDiesFirstGen(x, livingMap);
        }  cellSameFirstGen(x, set, livingMap);
    }

    private static void cellSameFirstGen(int x, Map<Integer, Integer> set, LivingMap livingMap) {
        livingMap.changeMap(x, set.get(x));
        FirstGeneration.checkCellNumber(--x, livingMap);
    }

    private static void cellLivesFirstGen(int x, LivingMap livingMap) {
        livingMap.changeMap(x, 1);
        FirstGeneration.checkCellNumber(--x, livingMap);
    }

    private static void cellDiesFirstGen(int x, LivingMap livingMap) {
        livingMap.changeMap(x, 0);
        FirstGeneration.checkCellNumber(--x, livingMap);
    }
}